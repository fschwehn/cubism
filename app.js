// dependencies
var express = require('express')
  , path = require('path')
  , favicon = require('serve-favicon')
  , logger = require('morgan')
  , cookieParser = require('cookie-parser')
  , bodyParser = require('body-parser')
  , mongoose = require('mongoose')
  , passport = require('passport')
  , expressSession = require('express-session')
  , MongoStore = require('connect-mongo')(expressSession)
  , LocalStrategy = require('passport-local').Strategy
  , User = require('./models/User')
  ;


// core vars
var app = express();
var sessionSecret = 'QVGDgsPZp8UWuZWhfaomoCFhAaHWufK7';
var databaseURI = process.env.MONGOLAB_URI
 || process.env.MONGOHQ_URL
 || 'mongodb://localhost/cubism';

// mongoose
var mongooseConnection = mongoose.connect(databaseURI).connections[0];

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// favicon
app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));

// logger
app.use(logger('dev'));

// parsers
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// session
app.use(expressSession({
    secret: sessionSecret,
    resave: false,
    saveUninitialized: false,
    // cookie: { secure: true },
    store: new MongoStore({ mongooseConnection: mongooseConnection})
}));
app.use(passport.initialize());
app.use(passport.session());


// // sass
// app.use(sassMiddleware({
//   src: __dirname + '/sass',
//   dest: __dirname + '/public/stylesheets',
//   prefix:  '/stylesheets',
//   debug: true
// }));

// static content
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bootstrap', express.static(path.join(__dirname, 'node_modules/bootstrap/dist')));

// routed content
app.use('/', require('./routes/index'));

// passport config
passport.use(new LocalStrategy({ usernameField: 'email' }, User.authenticate()));
User.conditionallyCreateRootAccount();
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
