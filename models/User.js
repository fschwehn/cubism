// dependencies
var mongoose = require('mongoose')
  , Schema = mongoose.Schema
  , passportLocalMongoose = require('passport-local-mongoose')
  ;

// root user defaults
var rootUsername = 'root'
  , initialRootPassword = 'please change that password'
  ;

// schema
var schema = new Schema({
    email: String,
    firstname: String,
    lastname: String,
    isAdmin: Boolean
});

schema.plugin(passportLocalMongoose);

// virtuals
var vName = schema.virtual('name');
vName.get(function () {
  var haveFirstname = !!this.firstname
    , haveLastname = !!this.lastname
    , haveName = haveFirstname || haveLastname
    ;
    
  if (haveName) {
    if (haveFirstname) {
      return this.firstname + ' ' + this.lastname;
    }
    else {
      return this.lastname;
    }
  }
  else {
    return this.email;
  }
});

var model = mongoose.model('User', schema);

model.conditionallyCreateRootAccount = function() {
  model.findOne({ username: rootUsername }, function (err, root) {
    if (err) return console.error(err);
    
    if (!root) {
      model.register(new model({
        username: rootUsername,
        firstname: 'root',
        lastname: 'of all evil',
        isAdmin: true
      }), initialRootPassword, function(err, account) {
        if (err) return console.error(err);
        
        console.log('successfully created root account', account);
      });
    }
  });
};

module.exports = model;
