var express = require('express')
  , _ = require('underscore')
  , s = require('underscore.string')
  , validator = require('validator')
  , router = express.Router()
  , User = require('../models/User.js')
  , log = require('debug').log
  ;

// LOCALS

router.use(function(req, res, next){
  _.extend(res.locals, {
    siteTitle: 'cubism',
    path: req.path,
    s: s,
    user: req.user
  });
  
  next();
});

// home page
router.get('/', function(req, res, next) {
  res.render('index', {
    pageTitle: 'Home'
  });
});

router.get('/algorithms', function(req, res, next) {
  res.render('algorithms', {
    pageTitle: 'Algorithms'
  });
});

router.get('/timer', function(req, res, next) {
  res.render('timer', {
    pageTitle: 'Timer'
  });
});

// authentication routes
router.use(require('./auth.js'));

module.exports = router;
