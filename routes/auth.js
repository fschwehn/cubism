var express = require('express')
  , _ = require('underscore')
  , s = require('underscore.string')
  , validator = require('validator')
  , router = express.Router()
  , User = require('../models/User.js')
  , passport = require('passport')
  ;

// SIGN UP

router.use('/signup', function(req, res, next) {
  var query = req.body;
  var locals = {
    pageTitle: 'Sign up',
    query: query
  };
  var render = function(error){
    locals.error = error;
    res.render('signup', locals);
  };
  
  if (req.method == 'GET') {
    render();
  }
  else {
    var error = null
      , email = query.email
      , password = query.password
      ;

    // email validation    
    if (!email) {
      error = 'Please enter your email address!';
    }
    else if (!validator.isEmail(email)) {
      error = 'No valid email address!';
    }
    
    // password validation
    else if (!password) {
      error = 'Please enter a password!';
    }
    else if (password.length < 6) {
      error = 'The password must contain at least 6 characters!';
    }
    else if (!validator.matches(password, /\d/)) {
      error = 'The password must contain at least one digit!';
    }
    else if (!validator.matches(password, /[^a-z0-9]/i)) {
      error = 'The password must contain at least one special character!';
    }
    
    if (error) {
      render(error);
    }
    else {
      User.register(new User({
        username: email,
        firstname: query.firstname,
        lastname: query.lastname,
        isAdmin: false
      }), password, function(err, user) {
        if (err) {
          if (err.name == 'BadRequestError') {
            render(err.message);
          }
          else {
            next(err);
          }
        }
        else {
          req.logIn(user, function(err) {
            if (err) {
              return next(err); 
            }
            
            return res.redirect('/');
          });
        }
      });
    }
  }
});

// LOG IN

router.use('/login', function (req, res, next) {
  var query = req.body;
  var render = function(){
    res.render('login', {
      pageTitle: 'Log in',
      query: query
    });
  };
  
  if (req.method == 'GET') {
    render();
  }
  else {
    console.log('here...');
    passport.authenticate('local', function(err, user, info) {
      console.log(err, user, info);
      if (err) {
        return next(err); 
      }
      if (!user) {
        query.error = info.message;
        return render();
      }
      req.logIn(user, function(err) {
        if (err) {
          return next(err); 
        }
        
        return res.redirect('/');
      });
    })(req, res, next);
  }
});

// LOG OUT

router.use('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});

module.exports = router;
